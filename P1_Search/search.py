# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """

    frontier = util.Stack() # Stack data structure
    expanded = set() # Set so it does not repeat itself
    frontier.push((problem.getStartState(), []))

    while not frontier.isEmpty():
        node, path = frontier.pop()

        if problem.isGoalState(node):
            # If the current node is the goal state, we've found a solution
            return path

        if node not in expanded:
           # If the current node has not been expanded before, expand it
            expanded.add(node)
            successors = problem.getSuccessors(node) # Get the successor states and actions

            for successor, way, _ in successors:
                # For each successor, add it to the frontier with the updated path
                frontier.push((successor, (path + [way])))


def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first."""

    frontier = util.Queue()  # Queue data structure
    expanded = set()  # Set so it does not repeat itself
    frontier.push((problem.getStartState(), [])) # Use a priority queue with our personalized heuristic

    while not frontier.isEmpty():
        node, path = frontier.pop()

        if problem.isGoalState(node):
            # If the current node is the goal state, we've found a solution
            return path

        if node not in expanded:
            # If the current node has not been expanded before, expand it
            expanded.add(node)
            successors = problem.getSuccessors(node)  # Get the successor states and actions

            for successor, action, _ in successors:
                # For each successor, add it to the frontier with the updated path
                frontier.push((successor, path + [action]))


def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""
    
    frontier = util.PriorityQueue() # Priority queue data structure, take the cost into acount
    expanded = set() # Set so it does not repeat itself
    frontier.push((problem.getStartState(), []), [])

    while not frontier.isEmpty():
        node, path = frontier.pop()
        total_cost = problem.getCostOfActions(path)

        if problem.isGoalState(node):
            # If the current node is the goal state, we've found a solution
            return path

        if node not in expanded:
           # If the current node has not been expanded before, expand it
            expanded.add(node)
            successors = problem.getSuccessors(node) # Get the successor states and actions

            for successor, way, cost in successors:
                # For each successor, add it to the frontier with the updated path and cost
                frontier.push((successor, (path + [way])), cost + total_cost)
    

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    
    # Define a personalized heuristic function that combines the provided heuristic and path cost
    def personalized_heuristic(state, heuristic): 
        return heuristic(state[0], problem) + problem.getCostOfActions(state[1])
    
    frontier = util.PriorityQueueWithFunction(personalized_heuristic) # Priority queue with personalized heuristic
    expanded = set() # Set so it does not repeat itself
    frontier.push((problem.getStartState(), []), heuristic)

    while not frontier.isEmpty():
        node, path = frontier.pop()

        if problem.isGoalState(node):
            # If the current node is the goal state, we've found a solution
            return path

        if node not in expanded:
            # If the current node has not been expanded before, expand it
            expanded.add(node)
            successors = problem.getSuccessors(node) # Get the successor states and actions

            for successor, way, _ in successors:
                # For each successor, add it to the frontier with the updated path and heuristic estimate
                frontier.push((successor, (path + [way])), heuristic)

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
